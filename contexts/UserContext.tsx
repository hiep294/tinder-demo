import { IUser } from "components/user/_interface";
import useUpdateEffect from "hooks/useUpdateEffect";
import {
  createContext,
  Dispatch,
  SetStateAction,
  useEffect,
  useState,
} from "react";

const UserContext = createContext({
  favouriteUsers: [] as IUser[],
  setFavouriteUsers: (undefined as unknown) as Dispatch<
    SetStateAction<IUser[]>
  >,
});

export const UserContextUtils = {
  setFavouriteUsers: (undefined as unknown) as Dispatch<
    SetStateAction<IUser[]>
  >,
  addFavouriteUser: (user: IUser) => {},
};

export const UserProvider = ({ children }) => {
  const [users, setUsers] = useState([]);
  const userStr = JSON.stringify(users);

  const onAddUser = (user: IUser) => {
    if (userStr.includes(`"seed":"${user.seed}"`)) return;
    setUsers((st) => [...st, user]);
  };
  UserContextUtils.setFavouriteUsers = setUsers;
  UserContextUtils.addFavouriteUser = onAddUser;

  useEffect(() => {
    try {
      // load list from local
      const listStr = localStorage.getItem("users") || "[]";
      const users = JSON.parse(listStr);
      setUsers(users);
    } catch (error) {
      console.log(error);
    }
  }, []);

  useUpdateEffect(() => {
    localStorage.setItem("users", userStr);
  }, [users]);

  return (
    <UserContext.Provider
      value={{
        favouriteUsers: users,
        setFavouriteUsers: setUsers,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserContext;
