import { UserProvider } from "contexts/UserContext";
import "../styles/main.scss";

function MyApp({ Component, pageProps }) {
  return (
    <UserProvider>
      <Component {...pageProps} />
    </UserProvider>
  );
}

export default MyApp;
