import PageWrapper from "components/page/PageWrapper";
import ListUser, {
  CardUserContainer,
  ListUserLayout,
} from "components/user/ListUser";
import ListUserFavourite from "components/user/ListUserFavourite";
import { IUser } from "components/user/_interface";
import Tabs, { TabsContent } from "components/_base/Tabs";
import { useEffect, useState } from "react";
import UserApi from "./api/userApi";
import Skeleton from "react-loading-skeleton";
import Box from "components/_base/Box";
import produce from "immer";
import { UserContextUtils } from "contexts/UserContext";

const Home = () => {
  const [tabActive, setTabActive] = useState("Swipe now");

  const [{ list, loading }, setState] = useState({
    list: [],
    loading: true,
  });

  useEffect(() => {
    const list = [] as IUser[];
    UserApi.getMany()
      .then((res) => {
        list.unshift(...res.data.results.reverse());
        return UserApi.getMany();
      })
      .then((res) => {
        list.unshift(...res.data.results.reverse());
        return UserApi.getMany();
      })
      .then((res) => {
        list.unshift(...res.data.results.reverse());
        return UserApi.getMany();
      })
      .then((res) => {
        list.unshift(...res.data.results.reverse());
        return UserApi.getMany();
      })
      .then((res) => {
        list.unshift(...res.data.results.reverse());
        setState({
          list,
          loading: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const addMoreUser = () => {
    const list = [] as IUser[];
    UserApi.getMany()
      .then((res) => {
        list.unshift(...res.data.results.reverse());
        setState((st) => {
          return produce(st, (draft) => {
            draft.list = [...list, ...draft.list];
          });
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <PageWrapper>
      <Tabs
        onClickItem={(item) => {
          setTabActive(item);
        }}
        tabActive={tabActive}
        list={["Swipe now", "Favourite"]}
        mb={1}
      />
      <TabsContent active={tabActive === "Swipe now"}>
        {loading ? (
          <ListUserLayout>
            <CardUserContainer>
              <Box center mb={7}>
                <Skeleton circle={true} height={"13rem"} width={"13rem"} />
              </Box>
              <Box mb={15}>
                <Skeleton count={2} />
              </Box>
              <Skeleton count={1} />
            </CardUserContainer>
          </ListUserLayout>
        ) : (
          <ListUser
            arr={list}
            handleItem={{
              onSwipeFinish: (isLiked, item) => {
                if (isLiked) {
                  UserContextUtils.addFavouriteUser(item);
                }
                setState((state) => {
                  const newList = state.list.filter(
                    (i) => i.seed !== item.seed
                  );
                  return { list: newList, loading: false };
                });

                addMoreUser();
              },
            }}
          />
        )}
      </TabsContent>
      <TabsContent active={tabActive === "Favourite"}>
        <ListUserFavourite />
      </TabsContent>
    </PageWrapper>
  );
};

export default Home;

const item = {
  user: {
    gender: "female",
    name: { title: "miss", first: "victoria", last: "oliver" },
    location: {
      street: "4674 timber wolf trail",
      city: "roseburg",
      state: "montana",
      zip: "20754",
    },
    email: "victoria.oliver98@example.com",
    username: "beautifulmouse226",
    password: "bird",
    salt: "tcvZ3gNF",
    md5: "1c3a108e1b9e8e0318c98dacbe0268b9",
    sha1: "67678e71a40da567e7ea58935e18f4a006d96008",
    sha256: "0aff20fbfe3bd624e907c554e93ae292ca8ecb94f6d4f25a59e9929a7eac0bff",
    registered: "923470299",
    dob: "323128400",
    phone: "(358)-923-6479",
    cell: "(367)-989-3751",
    SSN: "152-61-9286",
    picture: "http://api.randomuser.me/portraits/women/90.jpg",
  },
  seed: "5ff76e111a96598a1",
  version: "0.4",
} as IUser;

const seeds = [] as IUser[];

for (let index = 0; index < 20; index++) {
  seeds.push({
    ...item,
    seed: item.seed + index,
  });
}
