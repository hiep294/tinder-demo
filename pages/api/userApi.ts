import { AxiosResponse } from "axios";
import { IUser } from "components/user/_interface";
import { sendGet } from "./axios";

const UserApi = {
  getMany(): Promise<AxiosResponse<IResGetMany>> {
    return sendGet("https://randomuser.me/api/0.4/?randomapi");
  },
};

interface IResGetMany {
  results: IUser[];
}

export default UserApi;
