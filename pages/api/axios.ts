// put api here, because of recommanded Vercel

import Axios from "axios";

const axiosInstance = Axios.create({
  timeout: 3 * 60 * 1000,
});

export const sendGet = (url: string, params?: any) =>
  axiosInstance.get(url, { params: { ...params } });
