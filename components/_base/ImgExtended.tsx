import styled from "@emotion/styled";

const ImgExtended = styled.div<{
  scaleOnHover?: boolean;
  to?: string; // if html tag is Link
  rounded?: boolean;
  roundedCircle?: number;
  thumbnail?: boolean;
  fluid?: boolean;
}>`
  overflow: hidden;
  ${({ scaleOnHover }) =>
    scaleOnHover &&
    `
      & > img {
          transition: all 0.3s;
      }
      & > img:hover {
          transform: scale(1.1);
      }
  `}
  ${(props) =>
    props.rounded &&
    `
      border-radius: 0.4rem;
  `}
  ${({ roundedCircle }) =>
    roundedCircle &&
    `
      border-radius: 50%;
      & > img {
          object-fit: cover;
          border-radius: 50%;
          height: ${roundedCircle}rem;
          width: ${roundedCircle}rem;
      }
  `}
  ${(props) =>
    props.thumbnail &&
    `
      border: 1px solid #000;
      padding: 2px;
      & > img {
        border-radius: 50%;
      }
  `}
  ${(props) =>
    props.fluid &&
    `
      width: 100%;
      & > img {
          width: 100%;
      }
  `}
`;

export default ImgExtended;
