import styled from "@emotion/styled";
import Box from "./Box";

type Props = typeof Box.defaultProps & {
  list: string[];
  onClickItem: (_item: string) => any;
  tabActive: string;
};

const Tabs = ({
  list = [] as string[],
  onClickItem = (_item: string) => {},
  tabActive = "",
  ...others
}: Props) => {
  return (
    <TabList {...others}>
      {list.map((i) => (
        <TabItem
          key={i}
          active={i === tabActive}
          onClick={() => {
            onClickItem(i);
          }}
        >
          {i}
        </TabItem>
      ))}
    </TabList>
  );
};

export const TabsContent = ({ children, active = false }) => {
  return <div style={{ display: active ? undefined : "none" }}>{children}</div>;
};

const TabList = styled(Box)`
  display: flex;
  justify-content: center;
  & > *:not(:last-child) {
    border-right: none;
  }
`;

const TabItem = styled.div<{ active?: boolean }>`
  padding: 0.2rem 1.4rem;
  color: white;
  border: 2px solid white;
  cursor: pointer;
  font-weight: bold;
  ${({ active }) =>
    Boolean(active) &&
    `
    background-color: #f50057;
  `}
`;

export default Tabs;
