import styled from "@emotion/styled";
import Box from "./Box";

const IconExtended = styled(Box)<{
  active?: boolean;
}>`
  display: inline-block;
  cursor: pointer;
  transition: all 0.3s;
  border-top: 2px solid transparent;
  padding-top: 5px;
  position: relative;
  &:after {
    left: 50%;
    bottom: 100%;
    content: "";
    position: absolute;
    transform: translateX(-50%);
    transition: all 0.3s;
    border: 5px solid transparent;
  }
  ${({ active }) =>
    active === true &&
    `
    color: #4caf50;
    border-top-color: #4caf50;
    &:after {
      border-bottom-color: #4caf50;
    }
  `}
`;

export default IconExtended;
