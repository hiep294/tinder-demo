import styled from "@emotion/styled";

const Box = styled.div<{
  bordered?: boolean;
  my?: number;
  mt?: number;
  mx?: number;
  mr?: number;
  mb?: number;
  center?: boolean;
  flex?: string | number;
  minHeight?: number;
  direction?: "column";
}>`
  ${({ center }) =>
    center === true &&
    `display: flex; justify-content: center; align-items: center;`}
  ${({ flex }) => flex !== undefined && `flex: ${flex}`}
  ${({ mx }) =>
    mx !== undefined &&
    `
      margin-left: ${mx}rem;
      margin-right: ${mx}rem;
    `}
  ${({ my }) =>
    my !== undefined &&
    `
      margin-top: ${my}rem;
      margin-bottom: ${my}rem;
    `}
  ${({ bordered }) =>
    bordered &&
    `
      border: 1px solid var(--color-grey-light-24);
  `}
  ${({ mt }) =>
    mt !== undefined &&
    `
      margin-top: ${mt}rem;
  `}
  ${({ mr }) =>
    mr !== undefined &&
    `
      margin-right: ${mr}rem;
  `}
  ${({ mb }) =>
    mb !== undefined &&
    `
      margin-bottom: ${mb}rem;
  `}
  ${({ minHeight }) =>
    minHeight !== undefined &&
    `
      min-height: ${minHeight}rem;
  `}

  ${({ direction }) =>
    direction !== undefined &&
    `
    flex-direction: ${direction};
  `}
`;

export default Box;
