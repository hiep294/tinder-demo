import styled from "@emotion/styled";
import Box from "./Box";

type IProps = typeof IconExtendedStyled2.defaultProps & {
  round?: number;
  background?: string;
  children?: any;
  onClick?: () => any;
};

const IconExtended2 = (props: IProps) => {
  const { background, children, round, ...others } = props;

  return (
    <IconExtendedStyled2
      round={round}
      background={"transparent"}
      backgroundHover={"#e6e6e6"}
      backgroundActive={"#fbe3c4"}
      {...others}
    >
      {children}
    </IconExtendedStyled2>
  );
};

const IconExtendedStyled2 = styled(Box)<{
  round?: number;
  background?: string;
  backgroundHover?: string;
  backgroundActive?: string;
}>`
  display: inline-block;
  cursor: pointer;
  transition: background 0.1s;
  ${({ round }) =>
    round !== undefined &&
    `
    height: ${round}rem;
    width: ${round}rem;
    border-radius: 50%;
    display: inline-flex;
    align-items: center;
    justify-content: center;
  `}
  ${({ background }) =>
    background !== undefined &&
    `
    background: ${background};
  `}
  ${({ backgroundHover }) =>
    backgroundHover !== undefined &&
    `
    &:hover {
      background: ${backgroundHover};
    }
  `}
  ${({ backgroundActive }) =>
    backgroundActive !== undefined &&
    `
    &:active {
      background: ${backgroundActive};
    }
  `}
`;

export default IconExtended2;
