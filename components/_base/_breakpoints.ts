// to maintain, also maintain in assets/scss/breakpoints, maintain in Grid.tsx > Grid

export interface IBreakpoints {
  xs?: any;
  sm?: any;
  md?: any;
  lg?: any;
  xl?: any;
}

export interface IBreakpointsString {
  xs?: String;
  sm?: String;
  md?: String;
  lg?: String;
  xl?: String;
}

export interface IBreakpointsBoolean {
  xs?: boolean;
  sm?: boolean;
  md?: boolean;
  lg?: boolean;
  xl?: boolean;
}

export interface IBreakpointsNumber {
  xs?: Number;
  sm?: Number;
  md?: Number;
  lg?: Number;
  xl?: Number;
}

export const breakpoints: IBreakpoints = {
  xs: "0px",
  sm: "600px",
  md: "960px",
  lg: "1280px",
  xl: "1920px",
};

export default breakpoints;
