import styled from "@emotion/styled";

const Icon = styled.i<{ color?: string | "transparent"; fontSize?: number }>`
  ${({ color }) =>
    color !== undefined &&
    `
    color: ${color};
  `}
  ${({ fontSize }) =>
    fontSize !== undefined &&
    `
    font-size: ${fontSize}rem;
  `}
`;

export default Icon;
