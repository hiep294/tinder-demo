import styled from "@emotion/styled";
import { useEffect, useRef } from "react";

type Props = typeof ImgStyled.defaultProps & {
  ratio?: number; // width per height: 205 /175
};

const Img = (props: Props) => {
  const { ratio, ...others } = props;

  const ref = useRef(null);

  useEffect(() => {
    // effect
    // console.log(ref.current.id);
    const resizeHeight = () => {
      ref.current.style.height =
        Math.round(ref.current.offsetWidth / ratio) + "px";
    };
    if (ratio) window.addEventListener("resize", resizeHeight);
    if (ratio) resizeHeight();
    return () => {
      window.removeEventListener("resize", resizeHeight);
    };
  }, [ratio]);

  return <ImgStyled {...others} ref={ref} />;
};

const ImgStyled = styled.img<{
  theHeight?: number;
  theWidth?: number | string;
}>`
  object-fit: cover;
  object-position: center;
  ${({ theHeight: height }) =>
    height !== undefined &&
    `
    height: ${height}rem;
  `}
  ${({ theWidth: width }) => {
    if (typeof width === "number") return `width: ${width}rem`;
    if (typeof width === "string") return `width: ${width}`;
    return ``;
  }}
`;

export default Img;
