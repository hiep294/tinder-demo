import styled from "@emotion/styled";

const CardBody = styled.div`
  padding: 1.6rem;
`;
export default CardBody;
