import styled from "@emotion/styled";

const CardTitle = styled.h2<{ padding?: number | string }>`
  padding: 1.6rem;
  padding-bottom: 0;
  font-weight: normal;
  font-size: 2rem;
  ${({ padding }) => {
    if (typeof padding === "number")
      return `
      padding: ${padding}rem;
    `;
    return `padding: ${padding};`;
  }}
`;

export default CardTitle;
