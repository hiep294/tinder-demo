import styled from "@emotion/styled";
import Box from "../Box";

const Card = styled(Box)<{ background?: string }>`
  border: 1px solid #f0f0f0;
  background-color: white;
  box-shadow: 0px 2px 1px -1px rgb(0 0 0 / 20%),
    0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%);
  ${({ background }) => Boolean(background) && `background: ${background}`}
`;

export default Card;
