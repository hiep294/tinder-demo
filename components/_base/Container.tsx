import styled from "@emotion/styled";
import Box from "./Box";

const Container = styled(Box)<{ maxWidth?: number }>`
  margin-left: auto;
  margin-right: auto;
  padding-left: 1.5rem;
  padding-right: 1.5rem;
  max-width: ${(props) =>
    (props.maxWidth || 120) + 3}rem; // plus 3 rem for padding
`;

export default Container;
