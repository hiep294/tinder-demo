import styled from "@emotion/styled";
import Container from "components/_base/Container";

const PageWrapper = ({ children }) => {
  return (
    <PageWrapperLayout>
      <Container>{children}</Container>
    </PageWrapperLayout>
  );
};

const PageWrapperLayout = styled.div`
  min-height: 100vh;
  padding-top: 3rem;
  display: flex;
  background: linear-gradient(to top, #ccc 70%, #333 30%);
  justify-content: center;
  overflow: hidden;
`;

export default PageWrapper;
