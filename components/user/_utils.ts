import { IUser } from "./_interface";

export const concatFullname = (item: IUser) => {
  let rs = item?.user?.name?.first;
  const last = item?.user?.name?.last;
  if (last) {
    rs = rs + " " + last;
  }
  return rs;
};
