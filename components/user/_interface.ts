export interface IUser {
  seed: string;
  user: {
    SSN: string;
    cell: string;
    dob: string;
    email: string;
    gender: string;
    location: {
      street: string;
      city: string;
      state: string;
      zip: string;
    };
    md5: string;
    name: { title: string; first: string; last: string };
    password: string;
    phone: string;
    picture: string;
    registered: string;
    salt: string;
    sha1: string;
    sha256: string;
    username: string;
  };
}
