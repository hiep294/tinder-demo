import styled from "@emotion/styled";
import Icon from "components/_base/Icon";
import IconExtended2 from "components/_base/IconExtended2";
import UserContext from "contexts/UserContext";
import { useContext, useMemo } from "react";
import { CardUserContainer, ListUserLayout } from "./ListUser";
import { IUser } from "./_interface";
import { concatFullname } from "./_utils";

const ListUserFavourite = () => {
  const { favouriteUsers, setFavouriteUsers } = useContext(UserContext);

  const onClickIconTrash = (user: IUser) => {
    setFavouriteUsers((st) => st.filter((i) => i.seed !== user.seed));
  };
  return (
    <ListUserLayout>
      <CardUserContainer>
        {favouriteUsers.map((i) => (
          <UserItem
            key={i.seed}
            item={i}
            onClickIconTrash={() => {
              onClickIconTrash(i);
            }}
          />
        ))}
      </CardUserContainer>
    </ListUserLayout>
  );
};

const UserItem = ({ item = {} as IUser, onClickIconTrash = () => {} }) => {
  const fullname = concatFullname(item);
  const picture = item?.user?.picture;

  return useMemo(
    () => (
      <UserItemContainer>
        <Avatar src={picture} />
        <Name>{fullname}</Name>
        <div>
          <IconExtended2 round={3} onClick={onClickIconTrash}>
            <Icon className="icon-bin" fontSize={1.7} color="grey" />
          </IconExtended2>
        </div>
      </UserItemContainer>
    ),
    [item]
  );
};

const UserItemContainer = styled.div`
  display: flex;
  padding: 4px 18px 4px 16px;
  border-bottom: 1px solid #cccccc;
  justify-content: center;
  align-items: center;
  &:hover {
    background-color: rgba(0, 0, 0, 0.04);
  }
`;

const Avatar = styled.img`
  height: 4rem;
  width: 4rem;
  border-radius: 50%;
  object-fit: cover;
  object-position: center;
`;

const Name = styled.div`
  flex: 1;
  padding-left: 1rem;
  padding-right: 1rem;
`;

export default ListUserFavourite;
