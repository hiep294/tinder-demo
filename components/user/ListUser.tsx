import styled from "@emotion/styled";
import Box from "components/_base/Box";
import Card from "components/_base/Card";
import Icon from "components/_base/Icon";
import IconExtended from "components/_base/IconExtended";
import Img from "components/_base/Img";
import ImgExtended from "components/_base/ImgExtended";
import useUpdateEffect from "hooks/useUpdateEffect";
import { useMemo, useState } from "react";
import Draggable from "react-draggable";
import { IUser } from "./_interface";
import { concatFullname } from "./_utils";

type TProps = {
  arr: IUser[];
  handleItem: THandleItem;
};

type THandleItem = {
  onSwipeFinish: (isLiked: boolean, i: IUser) => any;
};

const ListUser = (props: TProps) => {
  const { arr } = props;

  return (
    <ListUserLayout>
      {arr.map((i, index) => (
        <User
          key={i.seed}
          isOnTopList={index === arr.length - 1}
          item={i}
          {...props.handleItem}
        />
      ))}
    </ListUserLayout>
  );
};

type TUserProps = THandleItem & {
  item: IUser;
  isOnTopList: boolean;
};

const User = ({ item, onSwipeFinish, isOnTopList }: TUserProps) => {
  const [state, setState] = useState({
    opacity: 1,
    position: {
      x: 0,
      y: 0,
    },
    isLiked: undefined as boolean | undefined,
  });
  const { opacity, position, isLiked } = state;
  const list = [
    {
      icon: "icon-user",
      propertyTitle: "My name is",
      propertyValue: concatFullname(item),
    },
    {
      icon: "icon-list",
      propertyTitle: "My gender is",
      propertyValue: item?.user?.gender,
    },
    {
      icon: "icon-map",
      propertyTitle: "My address is",
      propertyValue: item?.user?.location?.street,
    },
    {
      icon: "icon-phone",
      propertyTitle: "My phone number is",
      propertyValue: item?.user?.phone,
    },
    {
      icon: "icon-lock",
      propertyTitle: "My account is",
      propertyValue: item?.user?.username,
    },
  ];

  const [display, setDisplay] = useState(list[0]);

  const onStop = (e, data) => {
    const { x } = data;
    if (Math.abs(x) >= 200) {
      setState((st) => ({ ...st, opacity: 0, position: { x, y: 0 } }));
    } else {
      setState((st) => ({ ...st, position: { x: 0, y: 0 } }));
    }
  };

  const onDrag = (e, data) => {
    const { x, y } = data;
    if (x >= 200 && isLiked !== true) {
      setState((st) => ({ ...st, isLiked: true }));
    }
    if (x <= -200 && isLiked !== false) {
      setState((st) => ({ ...st, isLiked: false }));
    }
    if (x < 200 && x > -200 && isLiked !== undefined) {
      setState((st) => ({ ...st, isLiked: undefined }));
    }
  };

  useUpdateEffect(() => {
    setTimeout(() => {
      onSwipeFinish(isLiked, item);
    }, 200);
  }, [opacity]);

  return useMemo(
    () => (
      <Draggable
        axis="x"
        onDrag={onDrag}
        onStop={onStop}
        defaultPosition={{ x: 0, y: 0 }}
        position={position}
        grid={[25, 25]}
        scale={1}
      >
        <CardUserContainer
          background="linear-gradient(to top, #fff 70%, #fff 70.2%, #f5f5f5 30%);"
          opacity={opacity}
          boxShadow={!isOnTopList ? "none !important" : undefined}
        >
          <LikedContainer isLiked={isLiked}>
            <Liked isLiked={isLiked}>
              {isLiked ? "Like" : isLiked == false ? "Dislike" : null}
            </Liked>
          </LikedContainer>
          <Box center>
            <ImgExtended roundedCircle={13} thumbnail>
              <Img
                src={item?.user?.picture}
                style={{ pointerEvents: "none" }}
              />
            </ImgExtended>
          </Box>
          <InfoContainter>
            <UserProperty>{display.propertyTitle}</UserProperty>
            <UserPropertyValue>{display.propertyValue}</UserPropertyValue>
          </InfoContainter>
          <UserIconList>
            {list.map((i) => (
              <IconExtended
                key={i.icon}
                active={display.icon === i.icon}
                onClick={() => {
                  setDisplay(i);
                }}
              >
                <Icon className={i.icon} />
              </IconExtended>
            ))}
          </UserIconList>
        </CardUserContainer>
      </Draggable>
    ),
    [display, item, state, isOnTopList]
  );
};

// list
export const ListUserLayout = styled.div`
  height: calc(100vh - 20rem);
  width: 90vw;
  max-height: 60rem;
  max-width: 40rem;
  display: flex;
  position: relative;
  flex-direction: column;
`;

// item
export const CardUserContainer = styled(Card)<{
  opacity?: number;
  boxShadow?: string;
  index?: number;
}>`
  border-radius: 0.5rem;
  overflow-y: auto;
  flex: 1;
  transition: all 0.3s ease-out;
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 2rem;
  position: absolute;
  width: 100%;
  ${({ opacity }) => ({
    opacity,
  })}
  ${({ boxShadow }) => {
    return {
      boxShadow,
    };
  }}
`;

const InfoContainter = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const UserProperty = styled.p``;
const UserPropertyValue = styled.h2`
  text-transform: capitalize;
`;

const UserIconList = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 3rem;
  & > *:not(:last-child) {
    margin-right: 2rem;
  }
`;

// item - liked
const LikedContainer = styled.div<{ isLiked?: boolean }>`
  display: flex;
  position: absolute;
  ${({ isLiked }) => {
    if (isLiked) {
      return {
        justifyContent: "flex-end",
        right: "3rem",
        top: "3rem",
      };
    }
    return {
      justifyContent: "flex-start",
      left: "3rem",
      top: "3rem",
    };
  }}
`;
const Liked = styled.div<{ isLiked?: boolean }>`
  border: 2px solid transparent;
  color: transparent;
  transition: color 0.5s;
  padding: 0.1rem 1rem;
  ${({ isLiked }) => {
    if (isLiked) {
      return {
        borderColor: "green",
        color: "green",
      };
    }
    if (isLiked === false) {
      return {
        borderColor: "red",
        color: "red",
      };
    }
    return {};
  }}
`;

export default ListUser;
